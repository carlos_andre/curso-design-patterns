﻿using System;

namespace CursoDesignPatterns
{
    public class Item
    {
        public String Nome { get; set; }
        public Double Valor { get; set; }
        
        public Item(String nome, double valor)
        {
            this.Nome = nome;
            this.Valor = valor;
        }
    }
}