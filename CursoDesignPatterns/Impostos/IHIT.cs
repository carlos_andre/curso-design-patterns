﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns
{
    class IHIT : TemplateDeImpostoCondicional
    {
        public IHIT(Imposto outroImposto) : base(outroImposto)
        {
        }
        public IHIT()
        {

        }

        public override bool DeveUsarMaximaTaxacao(Orcamento orcamento)
        {
            foreach (var item in orcamento.Itens)
            {
                if (orcamento.Itens.Count(i => i.Nome.ToLower().Equals(item.Nome)) >= 2)
                    return true;
            }

            return false;
        }

        public override double MaximaTaxacao(Orcamento orcamento)
        {
            return (orcamento.Valor * 0.13)+100;

        }

        public override double MinimaTaxacao(Orcamento orcamento)
        {
            return orcamento.Valor * 0.01;
        }
    }
}
