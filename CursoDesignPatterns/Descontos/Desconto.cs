﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns
{
    public abstract class Desconto
    {
        public abstract Double Desconta(Orcamento orcamento);
        protected Desconto Proximo { get; set; } 
        public Desconto setProximo(Desconto desconto) {
            Proximo = desconto;
            return desconto;
        }
    }
}
