﻿using System;

namespace CursoDesignPatterns
{
    class DescontoPorCincoItens : Desconto
    {
        public override double Desconta(Orcamento orcamento)
        {
            if (orcamento.Itens.Count > 5)
            {
                return orcamento.Valor * 0.1;
            }

            return Proximo.Desconta(orcamento);
        }
    }
}
