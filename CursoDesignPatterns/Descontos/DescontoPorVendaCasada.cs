﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns
{
    class DescontoPorVendaCasada : Desconto
    {
        public override double Desconta(Orcamento orcamento)
        {
            if (orcamento.Existe("lápis") && orcamento.Existe("caneta"))
                return orcamento.Valor * 0.05;

            return Proximo.Desconta(orcamento);
        }
    }
}
