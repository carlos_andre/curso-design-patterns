﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns
{
    public class Orcamento
    {
        public Double Valor{ get; set; }
        public IList<Item> Itens { get; set; }

        public Orcamento(double valor)
        {
            this.Valor = valor;
            this.Itens = new List<Item>();

        }

        public void AdicionaItem(Item item)
        {
            Itens.Add(item);
        }

        public bool Existe(String NomeItem)
        {
            return this.Itens.Count(i => i.Nome.ToLower().Equals(NomeItem.ToLower())) > 0;
        }
    }
}
