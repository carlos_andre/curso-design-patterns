﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Imposto iss = new ISS(new ICCC(new ICMS(new IKCV(new IHIT(new ICPP())))));
            Orcamento orcamento = new Orcamento(500.0);
            double valor = iss.Calcula(orcamento);
            Console.WriteLine(valor);
            Console.ReadKey();
        }

        static void Aula02()
        {
            Imposto iss = new ISS();
            Imposto ICMS = new ICMS();

            Orcamento orcamento = new Orcamento(500.0);
            orcamento.AdicionaItem(new Item("Item 1", 50));
            orcamento.AdicionaItem(new Item("Caneta", 5));
            orcamento.AdicionaItem(new Item("Lapis", 1.5));

            CalculadorDeImpostos calculador = new CalculadorDeImpostos();
            CalculadorDeDescontos descontador = new CalculadorDeDescontos();

            double valorDesconto = descontador.Calcula(orcamento);

            Console.WriteLine($"Desconto: {valorDesconto}");
            Console.ReadKey();

        }
    }
}
